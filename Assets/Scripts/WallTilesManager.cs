using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTilesManager : MonoBehaviour
{
    [Range(1, 5)]
    [SerializeField] private int countOfEmptyTiles;
    [Range(0.1f, 40f)]
    [SerializeField] private float speed;
    [SerializeField] private WallTile tileEmpty;
    [SerializeField] private WallTile[] tiles;
    [SerializeField] private Transform transformForAdd;
    [SerializeField] private Transform transformForDelete;
    [SerializeField] private List<WallTile> wallTiles = new List<WallTile>();
    private int sch = 0;
    private bool isStarted = false;

    private void Update()
    {
        if (!isStarted) return;
        for (int i = 0; i < wallTiles.Count; i++)
        {
            wallTiles[i].transform.position += Vector3.down * Time.deltaTime * speed;
        }
        if (wallTiles[0].transform.position.y < transformForDelete.position.y)
        {
            Destroy(wallTiles[0].gameObject);
            wallTiles.RemoveAt(0);
        }
        if (wallTiles[wallTiles.Count - 1].TopPoint.position.y < transformForAdd.position.y)
        {
            WallTile tile = sch < countOfEmptyTiles ? tileEmpty : tiles[Random.Range(0, tiles.Length)];
            sch = sch >= countOfEmptyTiles ? 0 : sch + 1;
            AddTile(wallTiles[wallTiles.Count - 1].TopPoint.position, tile);
        }
    }

    public void Activate()
    {
        isStarted = true;
    }

    private void AddTile(Vector3 pos, WallTile tile)
    {
        WallTile newTile = Instantiate(tile.gameObject, pos, tile.transform.rotation, transform).GetComponent<WallTile>();
        wallTiles.Add(newTile);
    }
}
