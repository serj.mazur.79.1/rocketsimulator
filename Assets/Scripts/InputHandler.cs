using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public bool isPressed { get; private set; } = false;
    public float bias { get; private set; }
    [SerializeField] private Transform rocketTransform;
    [SerializeField] private Camera mainCamera;
    private Vector3 lastMousePos;

    private void Update()
    {
        if(isPressed == true && Input.GetMouseButton(0) == false)
        {
            isPressed = false;
            bias = 0f;
            return;
        }
        if (isPressed == false && Input.GetMouseButton(0) == true)
        {
            isPressed = true;
            lastMousePos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, rocketTransform.position.z - mainCamera.transform.position.z));
            bias = 0f;
            return;
        }
        if(isPressed == true)
        {
            Vector3 newMousePos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, rocketTransform.position.z - mainCamera.transform.position.z));
            bias = newMousePos.x - lastMousePos.x;
            lastMousePos = newMousePos;
        }
    }
}
