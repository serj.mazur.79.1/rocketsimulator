using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    public bool isLosed { get; private set; } = false;
    [SerializeField] private Transform topPoint;
    [SerializeField] private Transform bottomPoint;
    [Range(0.1f, 40f)]
    [SerializeField] private float speedTakeOff;
    [SerializeField] private Rocket rocket;
    private bool isPressed = false;
    private float bias;
    private State currentState = State.Idle;
    private Vector3 fallingRotation;

    private void Update()
    {
        if (rocket.isTouchedWall)
        {
            isLosed = true;
            return;
        }
        switch (currentState)
        {
            case State.Idle:
                if (isPressed)
                {
                    currentState = State.Takeoff;
                }
                break;
            case State.Fly:
                if (isPressed == false)
                {
                    rocket.SetGravity(true);
                    fallingRotation = new Vector3(Random.Range(-200f, 200f) * Time.deltaTime, Random.Range(-200f, 200f) * Time.deltaTime, Random.Range(-200f, 200f) * Time.deltaTime);
                    currentState = State.Falling;
                    break;
                }
                rocket.RocketTransform.rotation = Quaternion.Lerp(rocket.RocketTransform.rotation, rocket.startQuaternion, Time.deltaTime * 7f);
                rocket.RocketTransform.position += new Vector3(bias, 0f, 0f);
                break;
            case State.Takeoff:
                rocket.RocketTransform.rotation = Quaternion.Lerp(rocket.RocketTransform.rotation, rocket.startQuaternion, Time.deltaTime * 7f);
                rocket.RocketTransform.position += new Vector3(bias, 0f, 0f);
                if (isPressed == false)
                {
                    rocket.SetGravity(true);
                    fallingRotation = new Vector3(Random.Range(-200f, 200f) * Time.deltaTime, Random.Range(-200f, 200f) * Time.deltaTime, Random.Range(-200f, 200f) * Time.deltaTime);
                    currentState = State.Falling;
                    break;
                }
                if (rocket.RocketTransform.position.y < topPoint.position.y)
                {
                    rocket.RocketTransform.position += Vector3.up * Time.deltaTime * speedTakeOff;
                }
                else
                {
                    rocket.RocketTransform.position = new Vector3(rocket.RocketTransform.position.x, topPoint.position.y, rocket.RocketTransform.position.z);
                    currentState = State.Fly;
                }
                break;
            case State.Falling:
                if (rocket.RocketTransform.position.y <= bottomPoint.position.y)
                {
                    isLosed = true;
                    break;
                }
                if (isPressed)
                {
                    rocket.SetGravity(false);
                    currentState = State.Takeoff;
                    break;
                }
                rocket.RocketTransform.Rotate(fallingRotation);
                break;
        }
    }

    public void SetParams(bool isPressed, float bias)
    {
        this.isPressed = isPressed;
        this.bias = bias;
    }

    enum State
    {
        Idle,
        Fly,
        Takeoff,
        Falling
    }
}
