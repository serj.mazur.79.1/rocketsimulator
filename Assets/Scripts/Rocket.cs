using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    public bool isTouchedWall { get; private set; } = false;
    public Transform RocketTransform { get => rocketTransform; }
    public Quaternion startQuaternion { get; private set; }

    [SerializeField] private Transform rocketTransform;
    [SerializeField] private string wallTag;
    [SerializeField] private Rigidbody body;

    private void Start()
    {
        startQuaternion = RocketTransform.rotation;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Contains(wallTag))
        {
            isTouchedWall = true;
        }
    }

    public void SetGravity(bool param)
    {
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
        body.useGravity = param;
    }
}
