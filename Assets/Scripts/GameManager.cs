using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private WallTilesManager tilesManager;
    [SerializeField] private InputHandler inputHandler;
    [SerializeField] private RocketController rocketController;
    private bool isStarted = false;

    private void Update()
    {
        if (isStarted == false)
        {
            if (inputHandler.isPressed)
            {
                isStarted = true;
                rocketController.SetParams(inputHandler.isPressed, inputHandler.bias);
                tilesManager.Activate();
            }
            return;
        }
        if (rocketController.isLosed)
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
        rocketController.SetParams(inputHandler.isPressed, inputHandler.bias);
    }
}
